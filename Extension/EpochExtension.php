<?php

namespace XLabs\EpochBundle\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EpochExtension extends AbstractExtension
{
    private $container;
    private $xlabs_epoch_config;

    public function __construct(ContainerInterface $container, $xlabs_epoch_config)
    {
        $this->container = $container;
        $this->xlabs_epoch_config  = $xlabs_epoch_config;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('getEpochCamchargeUrl', array($this, 'getEpochCamchargeUrl')),
            new TwigFunction('getEpochReactivationUrl', array($this, 'getEpochReactivationUrl')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function getEpochCamchargeUrl($purchaseType, $purchase_id, $epoch_member_id, $amount, $referrer = false, $referrer_action = false)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if(is_string($user))
        {
            return 'javascript:;';
        }

        $params = array(
            'auth_amount' => $amount,
            'member_id' => $epoch_member_id,
            'returnurl' => $this->container->get('router')->generate('xlabs_epoch_response', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'pburl' => $this->container->get('router')->generate('xlabs_epoch_postback', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'x_purchaseType' => $purchaseType,
            'x_purchaseId' => $purchase_id
        );
        if($referrer)
        {
            $params['x_referrerUrl'] = $referrer;
        }
        if($referrer_action)
        {
            $params['x_referrerAction'] = $referrer_action;
        }

        $url = $this->container->get('xlabs_epoch_camcharge_api')->getUrl($params);

        return $url;
    }

    public function getEpochMemberplusUrl($purchaseType, $purchase_id, $subcategory_id, $epoch_member_id, $amount, $referrer = false, $referrer_action = false)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if(is_string($user))
        {
            return 'javascript:;';
        }

        $params = array(
            'auth_amount' => $amount,
            'member_id' => $epoch_member_id,
            'returnurl' => $this->container->get('router')->generate('xlabs_epoch_response', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'pburl' => $this->container->get('router')->generate('xlabs_epoch_postback', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'x_purchaseType' => $purchaseType,
            'x_purchaseId' => $purchase_id
        );
        if($referrer)
        {
            $params['x_referrerUrl'] = $referrer;
        }
        if($referrer_action)
        {
            $params['x_referrerAction'] = $referrer_action;
        }

        $url = $this->container->get('xlabs_epoch_memberplus_api')->getUrl($params);

        return $url;
    }

    public function getEpochReactivationUrl($epoch_member_id, $pi_code = false)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if(is_string($user))
        {
            return 'javascript:;';
        }

        $params = array(
            'member_id' => $epoch_member_id,
            'pi_returnurl' => $this->container->get('router')->generate('xlabs_epoch_response', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'pburl' => $this->container->get('router')->generate('xlabs_epoch_postback', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            //'x_purchaseType' => $purchaseType,
            //'x_purchaseId' => $purchase_id
        );
        if($pi_code)
        {
            $params['pi_code'] = $pi_code;
        }

        $url = $this->container->get('xlabs_epoch_reactivation_api')->getUrl($params);

        return $url;
    }
}