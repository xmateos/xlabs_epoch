<?php

namespace XLabs\EpochBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class XLabsEpochExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('xlabs_epoch_config', $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        // Setting the following parameters require to clear cache in order to update the current values
        /*$container->setParameter(
            'current_year',
            date('Y')
        );
        $container->setParameter(
            'current_month',
            date('m')
        );
        $container->setParameter(
            'current_day',
            date('d')
        );
        $container->setParameter(
            'current_date',
            date('Y-m-d')
        );
        $container->setParameter(
            'first_date_of_month',
            date('Y-m-01')
        );
        $container->setParameter(
            'last_date_of_month',
            date('Y-m-t')
        );*/
    }
}
