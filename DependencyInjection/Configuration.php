<?php

namespace XLabs\EpochBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_epoch');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->scalarNode('url')->isRequired()->end()
                ->scalarNode('postback_url')->defaultValue('/epoch/postback')->end()
                ->scalarNode('return_url')->defaultValue('/epoch/response')->end()
                ->scalarNode('stats_url')->defaultValue('/epoch/stats')->end()
                ->arrayNode('logging')->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultTrue()->end()
                        ->scalarNode('location')->defaultValue('%kernel.logs_dir%/')->end()
                    ->end()
                ->end()
                ->arrayNode('allowed_ips')->prototype('scalar')->defaultValue('[]')->end()->end()
                ->arrayNode('api')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('url')->defaultValue('https://wnu.com/secure/services/')->end()
                        ->scalarNode('reseller')->defaultValue('a')->end()
                        ->scalarNode('epoch_digest_key')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('mail_notifications')->addDefaultsIfNotSet()
                    ->treatFalseLike(array('enabled' => false))
                    ->treatTrueLike(array('enabled' => true))
                    ->treatNullLike(array('enabled' => false))
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('subject')->defaultValue('')->end()
                        ->scalarNode('from')->defaultValue('')->end()
                        ->arrayNode('destinataries')->prototype('scalar')->defaultValue('[]')->end()->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
