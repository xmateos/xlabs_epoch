<?php

namespace XLabs\EpochBundle\API;

use XLabs\EpochBundle\API\Exception\MissingMandatoryParamException;

class Reactivation
{
    private $config;
    private $errors;
    private $default_params;

    public function __construct($config)
    {
        $this->config = $config;
        $this->default_params = array(
            'api' => 'reactivate',
            'x_epochReactivation' => 1,
        );
        $this->errors = array();
    }

    public function getUrl($params)
    {
        $required_params = array(
            'member_id',
            'pi_returnurl',
            'pburl',
        );
        foreach($required_params as $required_param)
        {
            if(!isset($params[$required_param]))
            {
                throw new MissingMandatoryParamException('\''.$required_param.'\' mandatory param is missing.');
            }
        }
        $aParams = array_merge($this->default_params, $params);

        ksort($aParams);

        $sorted_string = http_build_query($aParams);
        $sorted_string = str_replace('&', '', $sorted_string);
        $sorted_string = str_replace('=', '', $sorted_string);
        $sorted_string = urldecode($sorted_string);

        $hash = hash_hmac('md5', $sorted_string, $this->config['api']['epoch_digest_key']);
        $aParams['epoch_digest'] = $hash;

        return $this->config['api']['url'].'?'.http_build_query($aParams);
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}

