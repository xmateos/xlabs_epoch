<?php

namespace XLabs\EpochBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\EpochBundle\Entity\CamChargeTrans;
use \DateTime;

class CamChargeTransRepository extends EntityRepository
{
    public function getPurchases($aOptions)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $default_options = array(
            'sorting' => 'date',
            'date_limit' => array(
                'min' => false,
                'max' => false
            ),
            'return_count' => false // if true, will only return the count instead of the rows
        );
        $aOptions = array_merge($default_options, $aOptions);

        if($aOptions['return_count'])
        {
            $qb->select('COUNT(DISTINCT cts.transaction_id) AS total_records');
            $qb->addSelect('SUM(cts.auth_amount) AS total_amount');
        } else {
            $qb->select('cts');
        }
        $qb
            ->from(CamChargeTrans::class, 'cts');

        switch($aOptions['sorting'])
        {
            case 'date':
                $qb
                    ->orderBy('cts.transaction_date','DESC');
                break;
        }

        if($aOptions['date_limit']['min'])
        {
            $min_date = new DateTime($aOptions['date_limit']['min']);
            $qb->andWhere(
                $qb->expr()->gte('DATE(cts.transaction_date)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00')))
            );
        }
        if($aOptions['date_limit']['max'])
        {
            $max_date = new DateTime($aOptions['date_limit']['max']);
            $qb->andWhere(
                $qb->expr()->lte('DATE(cts.transaction_date)', $qb->expr()->literal($max_date->format('Y-m-d 00:00:00')))
            );
        }

        $transactions = $qb->getQuery()->getArrayResult();

        return $aOptions['return_count'] ? $transactions[0] : $transactions;
    }

    public function getChartData($aOptions)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $default_options = array(
            'date_limit' => array(
                'min' => false,
                'max' => false
            )
        );
        $aOptions = array_merge($default_options, $aOptions);

        $min_date = new DateTime($aOptions['date_limit']['min']);
        $max_date = new DateTime($aOptions['date_limit']['max']);
        $qb
            ->select('COUNT(DISTINCT cts.transaction_id) AS total_records')
            ->addSelect('SUM(cts.auth_amount) as total_revenue')
            ->addSelect('SUBSTRING(cts.transaction_date, 9, 2) AS day')
            ->addSelect('SUBSTRING(cts.transaction_date, 6, 2) AS month')
            ->addSelect('SUBSTRING(cts.transaction_date, 1, 4) AS year')
            ->from(CamChargeTrans::class, 'cts')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->gte('DATE(cts.transaction_date)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00'))),
                    $qb->expr()->lte('DATE(cts.transaction_date)', $qb->expr()->literal($max_date->format('Y-m-d 23:59:59')))
                )
            )
            ->orderBy('year', 'ASC')
            ->addOrderBy('month', 'ASC')
            ->addOrderBy('day', 'ASC')
            ->groupBy('day')
            ->addGroupBy('month')
            ->addGroupBy('year');

        return $qb->getQuery()->getArrayResult();
    }
}