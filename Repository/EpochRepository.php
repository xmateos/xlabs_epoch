<?php

namespace XLabs\EpochBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\EpochBundle\Entity\EpochTransStats;
use \DateTime;

class EpochRepository extends EntityRepository
{
    /**
     * Return the original PI CODE used for the very first transaction
     */
    public function getReferrerPiCode($epoch_member_id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $first_purchase = $qb->select('ets')
            ->from(EpochTransStats::class, 'ets')
            ->where($qb->expr()->eq('ets.ets_member_idx', $epoch_member_id))
            ->orderBy('ets.ets_transaction_id', 'ASC')
            ->getQuery()->setMaxResults(1)->getArrayResult();
        return $first_purchase ? $first_purchase[0]['ets_pi_code'] : false;
    }

    /*public function getFirstPurchase($epoch_member_id)
    {
        if(empty($epoch_member_id))
        {
            return false;
        }
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $first_purchase = $qb->select('ets')
            ->from('CMSBillerBundle:Epoch\EpochTransStats', 'ets')
            ->where($qb->expr()->eq('ets.ets_member_idx', $epoch_member_id))
            ->orderBy('ets.ets_transaction_id', 'ASC')
            ->getQuery()->setMaxResults(1)->getArrayResult();
        return $first_purchase ? $first_purchase[0] : false;
    }

    public function getUserByEpochMemberID($epoch_member_id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $user = $qb->select('u')
            ->from('CMSUserBundle:User', 'u')
            ->where($qb->expr()->eq('u.biller_member_id', ':epoch_member_id'))
            ->setParameter('epoch_member_id', $epoch_member_id)
            ->getQuery()->getOneOrNullResult();
        return $user ? $user : false;
    }*/

    /*************************************************************************/
    /*                              STATS                                    */
    /*************************************************************************/
    public function getTransactions($aOptions)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $default_options = array(
            'transaction_type' => false,
            'sorting' => 'date',
            'date_limit' => array(
                'min' => false,
                'max' => false
            ),
            'having_channel' => false, // if true, will only return subcat transactions; if false, will exclude subcat transactions (channel related)
            'return_count' => false // if true, will only return the count instead of the rows
        );
        $aOptions = array_merge($default_options, $aOptions);

        if($aOptions['return_count'])
        {
            $qb->select('COUNT(DISTINCT ets.ets_transaction_id) AS total_records');
            $qb->addSelect('SUM(ets.ets_transaction_amount) AS total_amount');
        } else {
            $qb->select('ets');
        }
        $qb
            ->from(EpochTransStats::class, 'ets');

        if(is_array($aOptions['transaction_type']))
        {
            if(empty($aOptions['transaction_type']))
            {
                return $aOptions['return_count'] ? array(
                    'total_records' => 0,
                    'total_amount' => 0
                ) : array();
            } else {
                $qb->andWhere(
                    $qb->expr()->in('ets.ets_transaction_type', $aOptions['transaction_type'])
                );
            }
        }

        switch($aOptions['sorting'])
        {
            case 'date':
                $qb
                    ->orderBy('ets.ets_transaction_date','DESC');
                break;
        }

        if($aOptions['date_limit']['min'])
        {
            $min_date = new DateTime($aOptions['date_limit']['min']);
            $qb->andWhere(
                $qb->expr()->gte('DATE(ets.ets_transaction_date)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00')))
            );
        }
        if($aOptions['date_limit']['max'])
        {
            $max_date = new DateTime($aOptions['date_limit']['max']);
            $qb->andWhere(
                $qb->expr()->lte('DATE(ets.ets_transaction_date)', $qb->expr()->literal($max_date->format('Y-m-d 23:59:59')))
            );
        }

        if($aOptions['having_channel'])
        {
            $qb->andWhere(
                $qb->expr()->neq('ets.ets_site_subcat', $qb->expr()->literal(''))
            );
        } else {
            $qb->andWhere(
                $qb->expr()->eq('ets.ets_site_subcat', $qb->expr()->literal(''))
            );
        }

        $transactions = $qb->getQuery()->getArrayResult();

        return $aOptions['return_count'] ? $transactions[0] : $transactions;
    }

    public function getChartData($aOptions)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $default_options = array(
            'transaction_type' => false,
            'date_limit' => array(
                'min' => false,
                'max' => false
            ),
            'having_channel' => false, // if true, will only return subcat transactions; if false, will exclude subcat transactions (channel related)
        );
        $aOptions = array_merge($default_options, $aOptions);

        $min_date = new DateTime($aOptions['date_limit']['min']);
        $max_date = new DateTime($aOptions['date_limit']['max']);
        $qb
            ->select('COUNT(DISTINCT ets.ets_transaction_id) AS total_records')
            ->addSelect('SUM(ets.ets_transaction_amount) as total_revenue')
            ->addSelect('SUBSTRING(ets.ets_transaction_date, 9, 2) AS day')
            ->addSelect('SUBSTRING(ets.ets_transaction_date, 6, 2) AS month')
            ->addSelect('SUBSTRING(ets.ets_transaction_date, 1, 4) AS year')
            ->from(EpochTransStats::class, 'ets')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->in('ets.ets_transaction_type', $aOptions['transaction_type']),
                    $qb->expr()->gte('DATE(ets.ets_transaction_date)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00'))),
                    $qb->expr()->lte('DATE(ets.ets_transaction_date)', $qb->expr()->literal($max_date->format('Y-m-d 23:59:59')))
                )
            )
            ->orderBy('year', 'ASC')
            ->addOrderBy('month', 'ASC')
            ->addOrderBy('day', 'ASC')
            ->groupBy('day')
            ->addGroupBy('month')
            ->addGroupBy('year');

        if($aOptions['having_channel'])
        {
            $qb->andWhere(
                $qb->expr()->neq('ets.ets_site_subcat', $qb->expr()->literal(''))
            );
        } else {
            $qb->andWhere(
                $qb->expr()->eq('ets.ets_site_subcat', $qb->expr()->literal(''))
            );
        }

        return $qb->getQuery()->getArrayResult();
    }
}