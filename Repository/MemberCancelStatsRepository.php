<?php

namespace XLabs\EpochBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\EpochBundle\Entity\MemberCancelStats;
use \DateTime;

class MemberCancelStatsRepository extends EntityRepository
{
    public function getCancels($aOptions)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $default_options = array(
            'sorting' => 'date',
            'date_limit' => array(
                'min' => false,
                'max' => false
            ),
            'return_count' => false // if true, will only return the count instead of the rows
        );
        $aOptions = array_merge($default_options, $aOptions);

        if($aOptions['return_count'])
        {
            $qb->select($qb->expr()->countDistinct('mcs.mcs_or_idx'));
        } else {
            $qb->select('mcs');
        }
        $qb
            ->from(MemberCancelStats::class, 'mcs');

        switch($aOptions['sorting'])
        {
            case 'date':
                $qb
                    ->orderBy('mcs.mcs_canceldate','DESC');
                break;
        }

        if($aOptions['date_limit']['min'])
        {
            $min_date = new DateTime($aOptions['date_limit']['min']);
            $qb->andWhere(
                $qb->expr()->gte('DATE(mcs.mcs_canceldate)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00')))
            );
        }
        if($aOptions['date_limit']['max'])
        {
            $max_date = new DateTime($aOptions['date_limit']['max']);
            $qb->andWhere(
                $qb->expr()->lte('DATE(mcs.mcs_canceldate)', $qb->expr()->literal($max_date->format('Y-m-d 00:00:00')))
            );
        }

        $transactions = $qb->getQuery();

        return $aOptions['return_count'] ? $transactions->getSingleScalarResult() : $transactions->getArrayResult();
    }

    public function getChartData($aOptions)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $default_options = array(
            'date_limit' => array(
                'min' => false,
                'max' => false
            )
        );
        $aOptions = array_merge($default_options, $aOptions);

        $min_date = new DateTime($aOptions['date_limit']['min']);
        $max_date = new DateTime($aOptions['date_limit']['max']);
        $qb
            ->select('COUNT(DISTINCT mcs.mcs_or_idx) AS total_records')
            ->addSelect('0 as total_revenue')
            ->addSelect('SUBSTRING(mcs.mcs_canceldate, 9, 2) AS day')
            ->addSelect('SUBSTRING(mcs.mcs_canceldate, 6, 2) AS month')
            ->addSelect('SUBSTRING(mcs.mcs_canceldate, 1, 4) AS year')
            ->from(MemberCancelStats::class, 'mcs')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->gte('DATE(mcs.mcs_canceldate)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00'))),
                    $qb->expr()->lte('DATE(mcs.mcs_canceldate)', $qb->expr()->literal($max_date->format('Y-m-d 23:59:59')))
                )
            )
            ->orderBy('year', 'ASC')
            ->addOrderBy('month', 'ASC')
            ->addOrderBy('day', 'ASC')
            ->groupBy('day')
            ->addGroupBy('month')
            ->addGroupBy('year');

        return $qb->getQuery()->getArrayResult();
    }
}