<?php

namespace XLabs\EpochBundle\Services;

use XLabs\EpochBundle\Request\Request as EpochRequest;

class MailNotification
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function send(EpochRequest $epochRequest, $subject = false)
    {
        $aData = $epochRequest->getData();
        $querystring = $epochRequest->getQuerystring();

        if($this->config['mail_notifications']['enabled'])
        {
            $postback_url = $this->config['url'].$this->config['postback_url'].'?'.$querystring;

            // Subject
            $nSubject = 'Epoch postback: ';
            $nSubject .= $this->config['mail_notifications']['subject'] ? $this->config['mail_notifications']['subject'].' ' : '';
            $nSubject .= $subject ? $subject : '';

            // Body
            $nBody = '<br /><span style="text-align: center; background-color: #4DB24C; padding: 10px 15px; border-radius: 5px;"><a href="'.$postback_url.'" style="color: #fff; font-size: 14px; letter-spacing: 1px; text-decoration: none; text-shadow: 0px 2px 2px #4db24c; font-family: Arial, Helvetica, sans-serif;">Click me to resend postback</a></span><br /><br />';
            //$nBody .= '<a href="'.$postback_url.'" target="_blank">Resend postback</a><br /><br />';
            if(is_array($aData) && count($aData))
            {
                $nBody .= '<br /><b>Parameters</b>:<br />';
                foreach($aData as $key => $value)
                {
                    //$nBody .= $key.' <b>-></b> <i>'.$value.'</i><br />';
                    $nBody .= '<span style="margin-left: 30px;">'.$key.': <i>'.$value.'</i><span/><br />';
                }
            }
            $nBody .= '<br /><br /><b>Querystring</b>:<br />'.$querystring;

            // Headers
            $nHeaders  = 'MIME-Version: 1.0' . "\r\n";
            $nHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            if($this->config['mail_notifications']['from'] != '')
            {
                $nHeaders .= 'From: '.$this->config['mail_notifications']['from'] . "\r\n";
            }

            if(count($this->config['mail_notifications']['destinataries']))
            {
                foreach($this->config['mail_notifications']['destinataries'] as $recipient)
                {
                    mail($recipient, $nSubject, $nBody, $nHeaders);
                }
            }
        }
    }
}