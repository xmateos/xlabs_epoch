<?php

namespace XLabs\EpochBundle\Services;

use Monolog\Logger as BaseLogger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class Logger
{
    const LOGGER_CHANNEL = 'epoch_postback';
    const LOGGER_CHANNEL_ERROR = 'epoch_error';
    const LOGGER_CHANNEL_FIREWALL = 'epoch_firewall';
    private $config;
    private $logger;

    public function __construct($config)
    {
        $this->config = $config;
        $formatter = new LineFormatter(
            null, // Format of message in log, default [%datetime%] %channel%.%level_name%: %message% %context% %extra%\n
            null, // Datetime format
            true, // allowInlineLineBreaks option, default false
            true  // ignoreEmptyContextAndExtra option, default false
        );
        $this->logger = array(
            'postback' => new BaseLogger(self::LOGGER_CHANNEL),
            'error' => new BaseLogger(self::LOGGER_CHANNEL_ERROR),
            'firewall' => new BaseLogger(self::LOGGER_CHANNEL_ERROR),
        );
        $postback_handler = new StreamHandler($config['logging']['location'].self::LOGGER_CHANNEL.'.log', BaseLogger::DEBUG);
        $postback_handler->setFormatter($formatter);
        $this->logger['postback']->pushHandler($postback_handler);

        $error_handler = new StreamHandler($config['logging']['location'].self::LOGGER_CHANNEL_ERROR.'.log', BaseLogger::DEBUG);
        $error_handler->setFormatter($formatter);
        $this->logger['error']->pushHandler($error_handler);

        $firewall_handler = new StreamHandler($config['logging']['location'].self::LOGGER_CHANNEL_FIREWALL.'.log', BaseLogger::DEBUG);
        $firewall_handler->setFormatter($formatter);
        $this->logger['firewall']->pushHandler($firewall_handler);
    }

    public function info($msg)
    {
        if($this->config['logging']['enabled'])
        {
            $this->logger['postback']->info($msg);
        }
    }

    public function error($msg, $channel = 'error')
    {
        if($this->config['logging']['enabled'])
        {
            $this->logger[$channel]->error($msg);
        }
    }
}