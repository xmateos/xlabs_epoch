<?php

namespace XLabs\EpochBundle\Services;

class Firewall
{
    private $config;
    private $epoch_ips;

    public function __construct($config, $kernel_root_dir)
    {
        $this->config = $config;
        $this->epoch_ips = array();
        $epoch_ips_file = $kernel_root_dir.'/../web/epoch_allowed_ips.txt';
        if(file_exists($epoch_ips_file))
        {
            $this->epoch_ips = explode(', ', file_get_contents($epoch_ips_file));
        }
    }

    public function ip_in_range($ip, $range)
    {
        if(strpos($range, '/') == false)
        {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list($range, $netmask) = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
    }

    public function epochIpAllowed($ip = false)
    {
        //return true; // for testing
        $ip = $ip ? trim($ip) : trim($_SERVER['REMOTE_ADDR']);
        $allowed_ips = array($_SERVER['SERVER_ADDR']);
        $allowed_ips = array_merge($allowed_ips, $this->config['allowed_ips']);

        // Read allowed ips from file
        $allowed_ips = array_merge($allowed_ips, $this->epoch_ips);
        $allowed_ips = array_unique($allowed_ips);

        if(in_array($ip, $allowed_ips))
        {
            return true;
        } else {
            $allowed_cidr_blocks = array_filter($allowed_ips, function($v){
                if(strpos($v, '/'))
                {
                    return $v;
                }
            });
            foreach(array_filter($allowed_cidr_blocks) as $allowed_cidr_block)
            {
                if($this->ip_in_range($ip, $allowed_cidr_block))
                {
                    return true;
                }
            }
        }
        return false;
    }
}