<?php

namespace XLabs\EpochBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use DateTime;
use Symfony\Component\HttpFoundation\Response;
use XLabs\EpochBundle\Entity\EpochTransStats;
use XLabs\EpochBundle\Entity\CamChargeTrans;
use XLabs\EpochBundle\Entity\MemberCancelStats;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;
use Doctrine\DBAL\DBALException;
use XLabs\MMAdminBundle\Services\MMAdmin;

/**
 * @XLabsMMAdmin\isProtected
 */
class StatsController extends Controller
{
    public function indexAction()
    {
        return $this->forward('XLabsEpochBundle:Stats:indexByDate', array(
            'initDate' => date('Y-m-01'),
            'endDate' => date('Y-m-t')
        ));
    }

    public function indexByDateAction($initDate, $endDate)
    {
        $isMMAdmin = $this->get(MMAdmin::class)->isMMAdmin();

        if(!$isMMAdmin)
        {
            throw new AccessDeniedException('You are not allowed in here');
        }
        if(!$initDate || !$endDate)
        {
            $today = new DateTime();
            return $this->redirect($this->generateUrl('mm_global_model_stats', array(
                'initDate' => $today->format('Y-m-01'),
                'endDate' => $today->format('Y-m-t')
            )));
        }

        $currentDate = new DateTime();
        $currentYear = (int)$currentDate->format('Y');
        $initialYear = 2018;

        $yearsFilter = array();
        for($year = $currentYear; $year >= $initialYear; $year--)
        {
            $yearsFilter[] = $year;
        }

        $initPeriod = DateTime::createFromFormat('Y-m-d', $initDate);
        $endPeriod = DateTime::createFromFormat('Y-m-d', $endDate);
        return $this->render('XLabsEpochBundle:Stats:index.html.twig', array(
            'yearsFilter' => $yearsFilter,
            'initDate' => $initDate,
            'endDate' => $endDate,
            //'render_graphs' => $initPeriod->format('Y-m') != $endPeriod->format('Y-m') // only when listing more than one month
            'render_graphs' => true
        ));
    }

    public function getTotalsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();

        $initPeriod = DateTime::createFromFormat('Y-m-d', $initDate);
        $endPeriod = DateTime::createFromFormat('Y-m-d', $endDate);

        $totals = array();
        $totals['joins'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_STANDARD_INITIAL_RECURRING,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_PAID_TRIAL_ORDER,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_RECURRING,
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false,
            'return_count' => true
        ));
        $totals['channel_joins'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_STANDARD_INITIAL_RECURRING,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_PAID_TRIAL_ORDER,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_RECURRING,
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true,
            'return_count' => true
        ));
        $totals['rebills'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_TRIAL_CONVERSION,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_INITIAL_RECURRING
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false,
            'return_count' => true
        ));
        $totals['channel_rebills'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_TRIAL_CONVERSION,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_INITIAL_RECURRING
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true,
            'return_count' => true
        ));
        $totals['purchases'] = $em->getRepository(CamChargeTrans::class)->getPurchases(array(
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'return_count' => true
        ));
        $totals['refunds'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CREDIT_TO_CUSTOMERS_ACCOUNT
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false,
            'return_count' => true
        ));
        $totals['channel_refunds'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CREDIT_TO_CUSTOMERS_ACCOUNT
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true,
            'return_count' => true
        ));
        $totals['chargebacks'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CHARGEBACK
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false,
            'return_count' => true
        ));
        $totals['channel_chargebacks'] = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CHARGEBACK
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true,
            'return_count' => true
        ));
        $totals['cancels'] = $em->getRepository(MemberCancelStats::class)->getCancels(array(
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'return_count' => true
        ));

        return $this->render('XLabsEpochBundle:Stats:totals.html.twig', array(
            'initDate' => $initDate,
            'endDate' => $endDate,
            'totals' => $totals,
            'from' => $initPeriod->format('d-m-Y'),
            'to' => $endPeriod->format('d-m-Y')
        ));
    }

    /*
     * Joins
     */
    public function joinsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_STANDARD_INITIAL_RECURRING,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_PAID_TRIAL_ORDER,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_RECURRING,
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_joins');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/joins.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Channel Joins
     */
    public function channelJoinsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_STANDARD_INITIAL_RECURRING,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_PAID_TRIAL_ORDER,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_RECURRING,
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_channel_joins');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/channel_joins.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Rebills
     */
    public function rebillsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_TRIAL_CONVERSION,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_INITIAL_RECURRING
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_rebills');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/rebills.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Channel Joins
     */
    public function channelRebillsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_TRIAL_CONVERSION,
                EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_INITIAL_RECURRING
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_channel_rebills');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/channel_rebills.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Purchases
     */
    public function purchasesAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $purchases = $em->getRepository(CamChargeTrans::class)->getPurchases(array(
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            )
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $purchases,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_purchases');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/purchases.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Refunds
     */
    public function refundsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CREDIT_TO_CUSTOMERS_ACCOUNT
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_rebills');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/rebills.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Channel Refunds
     */
    public function channelRefundsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CREDIT_TO_CUSTOMERS_ACCOUNT
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_channel_refunds');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/channel_refunds.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Chargebacks
     */
    public function chargebacksAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CHARGEBACK
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => false
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_chargebacks');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/chargebacks.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Channel Chargebacks
     */
    public function channelChargebacksAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $transactions = $em->getRepository(EpochTransStats::class)->getTransactions(array(
            'transaction_type' => array(
                EpochTransStats::EPOCH_TRANSACTION_TYPE_CHARGEBACK
            ),
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            ),
            'having_channel' => true
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $transactions,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_channel_chargebacks');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/channel_chargebacks.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /*
     * Cancels
     */
    public function cancelsAction($initDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->get('page') ? $request->get('page') : 1;

        $purchases = $em->getRepository(MemberCancelStats::class)->getCancels(array(
            'date_limit' => array(
                'min' => $initDate,
                'max' => $endDate
            )
        ));

        $max_results = 25;
        $pagination = $this->get('knp_paginator')->paginate(
            $purchases,
            $page,
            $max_results
        );
        $transactions = $pagination->getItems();

        if($pagination)
        {
            $pagination->setUsedRoute('xlabs_epoch_stats_cancels');
            $pagination->setParam('initDate', $initDate);
            $pagination->setParam('endDate', $endDate);
        }

        return $this->render('XLabsEpochBundle:Stats:tables/cancels.html.twig', array(
            'transactions' => $transactions,
            'pagination' => $pagination,
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }
}