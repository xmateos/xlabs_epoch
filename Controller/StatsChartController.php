<?php

namespace XLabs\EpochBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use DateTime, DateInterval, DatePeriod;
use XLabs\EpochBundle\Entity\EpochTransStats;
use XLabs\EpochBundle\Entity\CamChargeTrans;
use XLabs\EpochBundle\Entity\MemberCancelStats;

class StatsChartController extends Controller
{
    public function renderAction($aOptions)
    {
        $entity = $aOptions['entity'];
        $initDate = $aOptions['date_limit']['min'];
        $endDate = $aOptions['date_limit']['max'];

        $currentDate = new DateTime();
        $currentYear = (int)$currentDate->format('Y');
        $joinYear = 2018; // when project started

        $yearsFilter = array();
        for($year = $currentYear; $year >= $joinYear; $year--)
        {
            $yearsFilter[] = $year;
        }

        // Data for chart
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $min_date = new DateTime($initDate);
        $min_date_clone = new DateTime($initDate);
        $max_date = new DateTime($endDate);
        $max_date_clone = new DateTime($endDate);

        // No graph if only 1 month picked
        /*if($min_date->format('Y-m') == $max_date->format('Y-m'))
        {
            return new Response('');
        }*/

        $graph_title = array(
            'by_total_transactions' => '',
            'by_total_revenue' => '',
        );
        $show_graph_by_revenue = true;
        switch($entity)
        {
            case 'EpochTransStats':
                $entity = EpochTransStats::class;
                $graph_title = array(
                    'by_total_transactions' => 'Transactions for '.$min_date->format('F Y').' to '.$max_date->format('F Y'),
                    'by_total_revenue' => 'Revenue for '.$min_date->format('F Y').' to '.$max_date->format('F Y'),
                );
                break;
            case 'CamChargeTrans':
                $entity = CamChargeTrans::class;
                $graph_title = array(
                    'by_total_transactions' => 'Purchases for '.$min_date->format('F Y').' to '.$max_date->format('F Y'),
                    'by_total_revenue' => 'Revenue for '.$min_date->format('F Y').' to '.$max_date->format('F Y'),
                );
                break;
            case 'MemberCancelStats':
                $entity = MemberCancelStats::class;
                $graph_title = array(
                    'by_total_transactions' => 'Cancels for '.$min_date->format('F Y').' to '.$max_date->format('F Y'),
                    'by_total_revenue' => '',
                );
                $show_graph_by_revenue = false;
                break;
        }
        if(!$entity)
        {
            return new Response('');
        }

        $x = $em->getRepository($entity)->getChartData($aOptions);

        $start = $min_date_clone->modify('first day of this month');
        $end = $max_date_clone->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($start, $interval, $end);
        $aDates = array();
        foreach($period as $dt)
        {
            $aDates[(int)$dt->format('Y')][(int)$dt->format('m')][(int)$dt->format('d')] = array(
                'total_transactions' => 0,
                'total_revenue' => 0,
            );
        }

        foreach($x as $data)
        {
            $aDates[(int)$data['year']][(int)$data['month']][(int)$data['day']] = array(
                'total_transactions' => (int)$data['total_records'],
                'total_revenue' => $data['total_revenue'],
            );
        }

        // Convert array for chartJS
        $aChartData = array(
            'by_total_transactions' => array(),
            'by_total_revenue' => array()
        );
        $chart_has_data = array(
            'by_total_transactions' => false,
            'by_total_revenue' => false
        );
        foreach($aDates as $year => $months)
        {
            foreach($months as $month => $days)
            {
                foreach($days as $day => $totals)
                {
                    $loopDate = new DateTime($day.'-'.$month.'-'.$year);
                    if($loopDate <= $currentDate)
                    {
                        $aChartData['by_total_transactions'][] = array(
                            'x' => 'new Date('.$year.', '.($month - 1).', '.$day.')',
                            'y' => $totals['total_transactions'],
                            'revenue' => $totals['total_revenue'], // custom var for graph tooltip
                            'markerColor' => count($aChartData['by_total_transactions']) && $aChartData['by_total_transactions'][count($aChartData['by_total_transactions']) - 1]['y'] > $totals['total_transactions'] ? '#F2464D' : '#4285F4',
                            //'markerImageUrl' => 'http://i.imgur.com/TUmQf5n.png'
                        );
                        $aChartData['by_total_revenue'][] = array(
                            'x' => 'new Date('.$year.', '.($month - 1).', '.$day.')',
                            'y' => (float)$totals['total_revenue'],
                            'yValueFormatString' => "$0.00",
                            'records' => $totals['total_transactions'], // custom var for graph tooltip
                            'markerColor' => count($aChartData['by_total_revenue']) && $aChartData['by_total_revenue'][count($aChartData['by_total_revenue']) - 1]['y'] > $totals['total_revenue'] ? '#F2464D' : '#A8D37B'
                        );

                        if($totals['total_transactions'] > 0)
                        {
                            $chart_has_data['by_total_transactions'] = true;
                            $chart_has_data['by_total_revenue'] = true;
                        }
                    }
                }
            }
        }

        $chart_data = array();
        if($chart_has_data['by_total_transactions'])
        {
            $chart_data['by_total_transactions'] = json_encode($aChartData['by_total_transactions'], JSON_PRETTY_PRINT);
            $chart_data['by_total_transactions'] = str_replace('"x"', 'x', $chart_data['by_total_transactions']);
            $chart_data['by_total_transactions'] = str_replace('"y"', 'y', $chart_data['by_total_transactions']);
            $chart_data['by_total_transactions'] = str_replace('"markerImageUrl"', 'markerImageUrl', $chart_data['by_total_transactions']);
            $chart_data['by_total_transactions'] = str_replace('"new Date(', 'new Date(', $chart_data['by_total_transactions']);
            $chart_data['by_total_transactions'] = str_replace(')"', ')', $chart_data['by_total_transactions']);
        } else {
            $chart_data['by_total_transactions'] = json_encode(array(), JSON_PRETTY_PRINT);
        }
        if($chart_has_data['by_total_revenue'])
        {
            $chart_data['by_total_revenue'] = json_encode($aChartData['by_total_revenue'], JSON_PRETTY_PRINT);
            $chart_data['by_total_revenue'] = str_replace('"x"', 'x', $chart_data['by_total_revenue']);
            $chart_data['by_total_revenue'] = str_replace('"y"', 'y', $chart_data['by_total_revenue']);
            $chart_data['by_total_revenue'] = str_replace('"markerImageUrl"', 'markerImageUrl', $chart_data['by_total_revenue']);
            $chart_data['by_total_revenue'] = str_replace('"new Date(', 'new Date(', $chart_data['by_total_revenue']);
            $chart_data['by_total_revenue'] = str_replace(')"', ')', $chart_data['by_total_revenue']);
        } else {
            $chart_data['by_total_revenue'] = json_encode(array(), JSON_PRETTY_PRINT);
        }

        return $this->render('XLabsEpochBundle:Stats:chart.html.twig', array(
            'graph_title' => $graph_title,
            'chart_data' => $chart_data,
            'show_graph_by_revenue' => $show_graph_by_revenue
        ));
    }

    /*public function revenueAction($initDate, $endDate, $affiliate_id)
    {
        $isMMAdmin = $this->get('xlabs_mm_admin')->isMMAdmin();
        $affiliate = $this->get('security.token_storage')->getToken()->getUser();

        if(!$isMMAdmin)
        {
            $affiliate_id = $affiliate->getId();
        }

        $currentDate = new DateTime();
        $currentYear = (int)$currentDate->format('Y');
        $joinYear = $isMMAdmin ? 2018 : (int)$affiliate->getJoindate()->format('Y');

        $yearsFilter = array();
        for($year = $currentYear; $year >= $joinYear; $year--)
        {
            $yearsFilter[] = $year;
        }

        // Data for chart
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $min_date = new DateTime($initDate);
        $min_date_clone = new DateTime($initDate);
        $max_date = new DateTime($endDate);
        $max_date_clone = new DateTime($endDate);

        // No graph if only 1 month picked
        if($min_date->format('Y-m') == $max_date->format('Y-m'))
        {
            return new Response('');
        }

        $qb->select('p.amount AS total_revenue')
            ->addSelect('SUBSTRING(p.creation_date, 6, 2) AS month')
            ->addSelect('SUBSTRING(p.creation_date, 1, 4) AS year')
            ->from('MMSalesBundle:Payment', 'p')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->gte('DATE(p.creation_date)', $qb->expr()->literal($min_date->format('Y-m-d 00:00:00'))),
                    $qb->expr()->lte('DATE(p.creation_date)', $qb->expr()->literal($max_date->format('Y-m-d 23:59:59')))
                )
            )
            ->orderBy('year', 'ASC')
            ->addOrderBy('month', 'ASC')
            ->groupBy('month')
            ->addGroupBy('year');
        if($affiliate_id)
        {
            $qb->join('p.affiliate','p_a','WITH', $qb->expr()->eq('p_a.id', $affiliate_id));
        }
        $x = $qb->getQuery()->getArrayResult();

        $start = $min_date_clone->modify('first day of this month');
        $end = $max_date_clone->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $aDates = array();
        foreach($period as $dt)
        {
            $aDates[(int)$dt->format('Y')][(int)$dt->format('m')] = 0;
        }

        foreach($x as $data)
        {
            $aDates[(int)$data['year']][(int)$data['month']] = (int)$data['total_revenue'];
        }

        // Convert array for chartJS
        $aChartData = array();
        foreach($aDates as $year => $months)
        {
            foreach($months as $month => $total_sales)
            {
                $aChartData[] = array(
                    'x' => 'new Date('.$year.', '.($month - 1).', 1)',
                    'y' => $total_sales
                );
            }
        }
        $chart_data = json_encode($aChartData, JSON_PRETTY_PRINT);
        $chart_data = str_replace('"x"', 'x', $chart_data);
        $chart_data = str_replace('"y"', 'y', $chart_data);
        $chart_data = str_replace('"new Date(', 'new Date(', $chart_data);
        $chart_data = str_replace('1)"', '1)', $chart_data);

        return $this->render('MMSalesBundle:Chart:revenue.html.twig', array(
            'title' => 'Revenue for '.$min_date->format('F Y').' to '.$max_date->format('F Y'),
            'chart_data' => $chart_data
        ));
    }*/
}