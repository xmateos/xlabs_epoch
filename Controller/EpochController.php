<?php

namespace XLabs\EpochBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use XLabs\EpochBundle\Request\Request as EpochRequest;
use XLabs\EpochBundle\Event\Response\Success;
use XLabs\EpochBundle\Event\Response\Failed;

class EpochController extends Controller
{
    public function responseAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $querystring = $request->getQueryString();
        $querystring = empty($querystring) ? http_build_query($request->request->all()) : $querystring;

        $epochRequest = new EpochRequest($querystring);

        if($epochRequest->isApproved())
        {
            $status = 'SUCCESS';
            $message = '';
            $e = new Success($epochRequest);
        } else {
            $status = 'ERROR';
            $message = $epochRequest->getDescriptiveError();
            $e = new Failed($epochRequest);
        }
        $this->get('event_dispatcher')->dispatch($e::NAME, $e);

        return $this->render('XLabsEpochBundle::response.html.twig', array(
            'status' => $status,
            'message' => $message
        ));
    }
}
