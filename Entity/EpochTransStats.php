<?php

namespace XLabs\EpochBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="EpochTransStats", indexes={
 *      @ORM\Index(
 *          name="idx_reseller",
 *          columns={"ets_reseller_code"}),
 *      @ORM\Index(
 *          name="idx_product",
 *          columns={"ets_pi_code"}),
 *      @ORM\Index(
 *          name="idx_transdate",
 *          columns={"ets_transaction_date"}),
 *      @ORM\Index(
 *          name="idx_type",
 *          columns={"ets_transaction_type"})
 * })
 * @ORM\Entity(repositoryClass="XLabs\EpochBundle\Repository\EpochRepository")
 */
class EpochTransStats
{
    const EPOCH_TRANSACTION_TYPE_CREDIT_TO_CUSTOMERS_ACCOUNT = 'C';
    const EPOCH_TRANSACTION_TYPE_CHARGEBACK = 'D';
    const EPOCH_TRANSACTION_TYPE_INITIAL_FREE_TRIAL = 'F';
    const EPOCH_TRANSACTION_TYPE_STANDARD_INITIAL_RECURRING = 'I';
    const EPOCH_TRANSACTION_TYPE_NON_INITIAL_RECURRING = 'N';
    const EPOCH_TRANSACTION_TYPE_NON_RECURRING = 'O';
    const EPOCH_TRANSACTION_TYPE_ONE_TIME_RELOAD = 'S';
    const EPOCH_TRANSACTION_TYPE_INITIAL_PAID_TRIAL_ORDER = 'T';
    const EPOCH_TRANSACTION_TYPE_INITIAL_TRIAL_CONVERSION = 'U';
    const EPOCH_TRANSACTION_TYPE_RETURNED_CHECK = 'X';
    const EPOCH_TRANSACTION_TYPE_REJECTED_CHECK = 'A';

    const EPOCH_PAYMENT_TYPE_CREDIT_CARD = 'A';
    const EPOCH_PAYMENT_TYPE_NON_CREDIT_CARD = 'B';

    /**
     * @ORM\Column(name="ets_transaction_id", type="bigint")
     * @ORM\Id
     */
    private $ets_transaction_id;

    /**
     * @ORM\Column(name="ets_member_idx", type="bigint")
     */
    private $ets_member_idx;

    /**
     * @ORM\Column(name="ets_transaction_date", type="datetime")
     */
    private $ets_transaction_date = null;

    /**
     * @ORM\Column(name="ets_transaction_type", type="string", length=1, nullable=false)
     */
    /*
        C = Credit to Customers Account
        D = Chargeback Transaction
        F = Initial Free Trial Transaction
        I = Standard Initial Recurring Transaction
        N = NonInitial Recurring Transaction
        O = NonRecurring Transaction
        S = One Time Re-Load Transaction
        T = Initial Paid Trial Order Transaction
        U = Initial Trial Conversion
        X = Returned Check Transaction
        A = Adjustments Initiated Internally / Rejected Check Transaction
     */
    private $ets_transaction_type = '';

    /**
     * @ORM\Column(name="ets_co_code", type="string", length=6, nullable=false)
     */
    private $ets_co_code = '';

    /**
     * @ORM\Column(name="ets_pi_code", type="string", length=32, nullable=false)
     */
    private $ets_pi_code = '';

    /**
     * @ORM\Column(name="ets_reseller_code", type="string", length=16)
     */
    private $ets_reseller_code = 'a';

    /**
     * @ORM\Column(name="ets_transaction_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $ets_transaction_amount = '0.00';

    /**
     * @ORM\Column(name="ets_payment_type", type="string", length=1)
     */
    /*
        V = Visa
        M = MasterCard
        O = Other
        B = ACH
     */
    private $ets_payment_type = 'A';

    /**
     * @ORM\Column(name="ets_pst_type", type="string", length=2, nullable=false)
     */
    private $ets_pst_type = '';

    /**
     * @ORM\Column(name="ets_username", type="string", length=32)
     */
    private $ets_username = null;

    /**
     * @ORM\Column(name="ets_ref_trans_ids", type="bigint")
     */
    private $ets_ref_trans_ids = null;

    /**
     * @ORM\Column(name="ets_password_expire", type="string", length=20)
     */
    private $ets_password_expire = null;

    /**
     * @ORM\Column(name="ets_email", type="string", length=64)
     */
    private $ets_email = null;

    /**
     * @ORM\Column(name="ets_site_subcat", type="string", length=64)
     */
    private $ets_site_subcat = null;

    public function __construct()
    {

    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}

