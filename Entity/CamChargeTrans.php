<?php

namespace XLabs\EpochBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Table(name="CamChargeTrans")
 * @ORM\Entity(repositoryClass="XLabs\EpochBundle\Repository\CamChargeTransRepository")
 */
class CamChargeTrans
{
    /*
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    //private $id;

    /**
     * @ORM\Column(name="transaction_id", type="bigint")
     * @ORM\Id
     */
    private $transaction_id;

    /**
     * @ORM\Column(name="transaction_date", type="datetime")
     */
    private $transaction_date;

    /**
     * @ORM\Column(name="ans", type="string", length=64)
     */
    private $ans;

    /**
     * @ORM\Column(name="auth_code", type="string", length=64)
     */
    private $auth_code;

    /**
     * @ORM\Column(name="member_id", type="bigint")
     */
    private $member_id;

    /**
     * @ORM\Column(name="currency", type="string", length=12)
     */
    private $currency;

    /**
     * @ORM\Column(name="auth_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $auth_amount = '0.00';

    /**
     * @ORM\Column(name="ip", type="string", length=128)
     */
    private $ip;

    /**
     * @ORM\Column(name="action", type="string", length=24)
     */
    private $action;

    /**
     * @ORM\Column(name="x_purchaseType", type="string", length=128)
     */
    private $x_purchaseType;

    /**
     * @ORM\Column(name="x_purchaseId", type="string", length=128)
     */
    private $x_purchaseId;

    public function __construct()
    {
        $this->transaction_date = new DateTime();
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}

