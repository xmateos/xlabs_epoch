<?php

namespace XLabs\EpochBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="MemberCancelStats")
 * @ORM\Entity(repositoryClass="XLabs\EpochBundle\Repository\MemberCancelStatsRepository")
 */
class MemberCancelStats
{
    /**
     * @ORM\Column(name="mcs_or_idx", type="bigint", nullable=false)
     * @ORM\Id
     */
    private $mcs_or_idx;

    /**
     * @ORM\Column(name="mcs_canceldate", type="datetime")
     */
    private $mcs_canceldate = null;

    /**
     * @ORM\Column(name="mcs_signupdate", type="datetime")
     */
    private $mcs_signupdate = null;

    /**
     * @ORM\Column(name="mcs_cocode", type="string", length=16, nullable=false)
     */
    private $mcs_cocode = '';

    /**
     * @ORM\Column(name="mcs_picode", type="string", length=32, nullable=false)
     */
    private $mcs_picode = '';

    /**
     * @ORM\Column(name="mcs_reseller", type="string", length=16)
     */
    private $mcs_reseller = null;

    /**
     * @ORM\Column(name="mcs_reason", type="string", length=64)
     */
    private $mcs_reason = null;

    /**
     * @ORM\Column(name="mcs_memberstype", type="string", length=1)
     */
    private $mcs_memberstype = null;

    /**
     * @ORM\Column(name="mcs_username", type="string", length=32)
     */
    private $mcs_username = null;

    /**
     * @ORM\Column(name="mcs_email", type="string", length=64)
     */
    private $mcs_email = null;

    /**
     * @ORM\Column(name="mcs_passwordremovaldate", type="datetime")
     */
    private $mcs_passwordremovaldate = null;

    public function __construct()
    {

    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}

