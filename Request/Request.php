<?php

namespace XLabs\EpochBundle\Request;

class Request
{
    private $querystring;
    private $data;

    public function __construct($querystring)
    {
        $this->querystring = $querystring;
        parse_str($this->querystring, $this->data);
    }

    public function __get($name)
    {
        if(array_key_exists($name, $this->data))
        {
            return $this->data[$name];
        }
        return false;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getQuerystring()
    {
        return $this->querystring;
    }

    public function isApproved()
    {
        // this parameter is only for flexpost (initial postbacks), not dataplus
        $data = $this->data;
        if(isset($data['ans']))
        {
            $bankAnswer = str_split($data['ans']);
            return ($bankAnswer[0] == 'Y');
        }
        return false;
    }

    public function isTestTransaction()
    {
        $data = $this->data;
        if(isset($data['ans']))
        {
            return (strpos($data['ans'], 'YGOODTEST') !== false);
        }
        return false;
    }

    public function getDescriptiveError()
    {
        $bankAnswer = isset($data['ans']) ? $this->data['ans'] : '';
        $bankError = explode(',', $bankAnswer);
        $bankError = $bankError[0];
        $arrErrors = array(
            'NMYNEGDB' => 'Internal Denial',
            'NMYCEH' => 'Customer already has a membership',
            'NDECLINED' => 'Declined by bank',
            'NMYBLOCKEDZIP' => 'Postal code blocked',
            'NMYSERVNOTALLOWED' => 'Service not allowed',
            'NMYDNVEL' => 'Too many denials',
            'NMYNOTACCEPTED' => 'Payment type not accepted',
            'NDECLINEDE' => 'Declined by the bank',
            'NMYVSNOTACCEPTED' => 'Visa not accepted for the site',
            'NMYMCNOTACCEPTED' => 'Mastercard not accepted for the site',
            'NMYSWNOTACCEPTED' => 'Switch not accepted for the site',
            'NMYSONOTACCEPTED' => 'Solo not accepted for the site',
            'NMYDSNOTACCEPTED' => 'Discover not accepted for the site',
            'NMYDDNOTACCEPTED' => 'Direct Debit not accepted',
            'NMYACNOTACCEPTED' => 'ACH not accepted',
            'NMYINVINFO' => 'NMYINVINFOOne of the required parameter contains an invalid value',
            'NINVCVV2' => 'Invalid CVV2',
            'NMYDENIED' => 'Denied',
            'NMYDUPLICATE' => 'Duplicate membership',
            'NMYIDNOTACCEPTED' => 'ideal not accepted',
            'NMYGPNOTACCEPTED' => 'Giropay not accepted',
            'NMYPNNOTACCEPTED' => 'eBanking not accepted',
            'NMYDINOTACCEPTED' => 'Dutch Incasso not accepted',
            'NMYDCNOTACCEPTED' => 'Diners card not accepted',
            'NMYJCNOTACCEPTED' => 'JCB not accepted',
            'NCALLCENTER' => 'Declined by bank',
            'NDECLINEDINSFUNDS' => 'Declined by bank',
            'NMYRETRY' => 'There was a problem, please try again',
            'NMYINVAMT' => 'Declined by bank',
            'NINVDATA' => 'Some of the data is invalid',
            'NDECLINEDCVV2' => 'Declined by bank',
        );
        //return 'Error code '.$bankError.': '.(array_key_exists($bankError, $arrErrors) ? $arrErrors[$bankError] : '~');
        return array_key_exists($bankError, $arrErrors) ? $arrErrors[$bankError] : '~';
    }
}