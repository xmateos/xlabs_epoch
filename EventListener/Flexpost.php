<?php

namespace XLabs\EpochBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\EpochBundle\Services\Logger;
use XLabs\EpochBundle\Services\MailNotification as EpochMailNotification;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\EpochBundle\Event\Sync\Join;

class Flexpost extends Event
{
    private $em;
    private $logger;
    private $mail_notification;
    private $event_dispatcher;

    public function __construct(EntityManagerInterface $em, Logger $logger, EpochMailNotification $mail_notification, EventDispatcherInterface $event_dispatcher)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
        $this->event_dispatcher = $event_dispatcher;
    }
    
    public function onFlexpostPostback(Event $event)
    {
        $epochRequest = $event->getParams(); // XLabs\EpochBundle\Request\Request.php
        $params = $epochRequest->getData();

        $e = new Join($params);
        $epoch_user = array_key_exists('username', $params) ? $params['username'] : '';
        $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' join (Flexpost SYNC)');

        $this->event_dispatcher->dispatch($e::NAME, $e);
        $this->logger->info('Event '.$e::NAME.' dispatched.');

        $event_response = $e->getResponse();
        if($event_response)
        {
            $event->setResponse($event_response);
        }
    }
}