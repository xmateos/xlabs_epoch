<?php

namespace XLabs\EpochBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\EpochBundle\Services\Logger;
use XLabs\EpochBundle\Services\MailNotification as EpochMailNotification;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\EpochBundle\Entity\CamChargeTrans;
use XLabs\EpochBundle\Event\Sync\Purchase;

class Camcharge extends Event
{
    private $em;
    private $logger;
    private $mail_notification;
    private $event_dispatcher;

    public function __construct(EntityManagerInterface $em, Logger $logger, EpochMailNotification $mail_notification, EventDispatcherInterface $event_dispatcher)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
        $this->event_dispatcher = $event_dispatcher;
    }
    
    public function onCamchargePostback(Event $event)
    {
        $epochRequest = $event->getParams(); // XLabs\EpochBundle\Request\Request.php
        $params = $epochRequest->getData();

        // CamChargeTrans
        $transaction = new CamChargeTrans();
        foreach($params as $key => $value)
        {
            if(property_exists($transaction, $key))
            {
                $transaction->__set($key, $value);
            }
        }
        $this->em->persist($transaction);
        $this->logger->info('CamCharge transaction: '.$transaction->__get('transaction_id'));

        try {
            $this->em->flush();
            $this->logger->info('CamCharge '.$transaction->__get('transaction_id').' stored successfully.');

            // individual charges, like buying tokens
            $e = new Purchase($params);
            $this->mail_notification->send($epochRequest, 'user purchase (Camcharge SYNC)');

            $this->event_dispatcher->dispatch($e::NAME, $e);
            $this->logger->info('Event '.$e::NAME.' dispatched.');

            $event_response = $e->getResponse();
            if($event_response)
            {
                $event->setResponse($event_response);
            }
        } catch (\Exception $exception) {
            // Probably transaction already exists
            $this->logger->error('CamCharge transaction '.$transaction->__get('transaction_id').' not stored: '.$exception->getMessage(), 'postback');
        }
    }
}