<?php

namespace XLabs\EpochBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\EpochBundle\Services\Logger;
use XLabs\EpochBundle\Services\MailNotification as EpochMailNotification;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\EpochBundle\Entity\EpochTransStats;
use XLabs\EpochBundle\Entity\MemberCancelStats;
use XLabs\EpochBundle\Event\Async\Join;
use XLabs\EpochBundle\Event\Async\JoinChannel;
use XLabs\EpochBundle\Event\Async\Rebill;
use XLabs\EpochBundle\Event\Async\RebillChannel;
use XLabs\EpochBundle\Event\Async\Refund;
use XLabs\EpochBundle\Event\Async\RefundChannel;
use XLabs\EpochBundle\Event\Async\Chargeback;
use XLabs\EpochBundle\Event\Async\ChargebackChannel;
use XLabs\EpochBundle\Event\Async\Cancel;
use XLabs\EpochBundle\Event\Async\Purchase;
use DateTime;

class Dataplus extends Event
{
    private $em;
    private $logger;
    private $mail_notification;
    private $event_dispatcher;

    public function __construct(EntityManagerInterface $em, Logger $logger, EpochMailNotification $mail_notification, EventDispatcherInterface $event_dispatcher)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
        $this->event_dispatcher = $event_dispatcher;
    }
    
    public function onDataplusPostback(Event $event)
    {
        $epochRequest = $event->getParams(); // XLabs\EpochBundle\Request\Request.php
        $params = $epochRequest->getData();
        $e = false;

        // EpochTransStats
        if(isset($params['ets_pi_code']))
        {
            $transaction = new EpochTransStats();
            foreach($params as $key => $value)
            {
                if(property_exists($transaction, $key))
                {
                    if($key == 'ets_transaction_date')
                    {
                        $value = DateTime::createFromFormat('Y-m-d H:i:s', $value);
                        $value = $value ? $value : new DateTime($value); // to fix issues with ISO 8601 datetimes
                    }
                    if($key == 'ets_password_expire')
                    {
                        $value = DateTime::createFromFormat('Y-m-d H:i:s', $value);
                        $value = $value ? $value : new DateTime($value); // to fix issues with ISO 8601 datetimes
                        $value = $value->format('Y-m-d H:i:s');
                    }
                    $transaction->__set($key, $value);
                }
            }
            $this->em->persist($transaction);
            $this->logger->info('DataPlus transaction: '.$transaction->__get('ets_transaction_id'));

            try {
                $this->em->flush();
                $this->logger->info('Transaction '.$transaction->__get('ets_transaction_id').' stored successfully.');

                $epoch_user = array_key_exists('ets_username', $params) ? $params['ets_username'] : '';
                switch($params['ets_transaction_type'])
                {
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_CREDIT_TO_CUSTOMERS_ACCOUNT: // refund
                        if(isset($params['ets_site_subcat']) && $params['ets_site_subcat'] != '')
                        {
                            $e = new RefundChannel($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' refund channel (Dataplus ASYNC)');
                        } else {
                            $e = new Refund($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' refund (Dataplus ASYNC)');
                        }
                        break;
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_CHARGEBACK: // chargeback
                        if(isset($params['ets_site_subcat']) && $params['ets_site_subcat'] != '')
                        {
                            $e = new ChargebackChannel($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' chargeback channel (Dataplus ASYNC)');
                        } else {
                            $e = new Chargeback($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' chargeback (Dataplus ASYNC)');
                        }
                        break;
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_STANDARD_INITIAL_RECURRING: // initial
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_PAID_TRIAL_ORDER: // initial as trial (depends on the pi_code)
                        if(isset($params['ets_site_subcat']) && $params['ets_site_subcat'] != '')
                        {
                            $e = new JoinChannel($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' join channel (Dataplus ASYNC)');
                        } else {
                            $e = new Join($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' join (Dataplus ASYNC)');
                        }
                        break;
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_RECURRING: // for instance for zero charge (lifetime) join packages
                        if(isset($params['ets_site_subcat']) && $params['ets_site_subcat'] != '')
                        {
                            $e = new JoinChannel($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' non-recurring join channel (Dataplus ASYNC)');
                        } else {
                            $e = new Join($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' non-recurring join (Dataplus ASYNC)');
                        }
                        break;
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_INITIAL_TRIAL_CONVERSION: // rebill from trial (depends on the pi_code)
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_NON_INITIAL_RECURRING: // rebill
                        if(isset($params['ets_site_subcat']) && $params['ets_site_subcat'] != '')
                        {
                            $e = new RebillChannel($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' rebill channel (Dataplus ASYNC)');
                            break;
                        } else {
                            $e = new Rebill($params);
                            $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' rebill (Dataplus ASYNC)');
                            break;
                        }
                    case EpochTransStats::EPOCH_TRANSACTION_TYPE_ONE_TIME_RELOAD: // one-time (non-recurring) transaction (i.e. camcharge tokens buying)
                        $e = new Purchase($params);
                        $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' one-time transaction (Dataplus ASYNC)');
                        break;
                    default:
                        $e = false;
                        $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' unknown postback (Dataplus UNKNOWN)');
                        break;
                }
            } catch (\Exception $exception) {
                // Probably transaction already exists
                $this->logger->error('Transaction '.$transaction->__get('ets_transaction_id').' not stored: '.$exception->getMessage(), 'postback');
            }
        }

        // MemberCancelStats
        if(isset($params['mcs_or_idx']))
        {
            $cancellation = new MemberCancelStats();
            foreach($params as $key => $value)
            {
                if(property_exists($cancellation, $key))
                {
                    //if($key == 'mcs_canceldate' || $key == 'mcs_signupdate' || $key == 'mcs_passwordremovaldate')
                    if(in_array($key, array('mcs_canceldate', 'mcs_signupdate', 'mcs_passwordremovaldate')))
                    {
                        $value = DateTime::createFromFormat('Y-m-d H:i:s', $value);
                        $value = $value ? $value : new DateTime($value);
                    }
                    $cancellation->__set($key, $value);
                }
            }
            $this->em->persist($cancellation);
            $this->logger->info('DataPlus cancellation: '.$cancellation->__get('mcs_or_idx'));

            try {
                $this->em->flush();
                $this->logger->info('Cancellation '.$cancellation->__get('mcs_or_idx').' stored successfully.');

                $epoch_user = array_key_exists('mcs_username', $params) ? $params['mcs_username'] : '';
                $e = new Cancel($params);
                $this->mail_notification->send($epochRequest, 'user '.$epoch_user.' cancel (Dataplus ASYNC)');
            } catch (\Exception $exception) {
                // Probably transaction already exists
                $this->logger->error('Cancellation '.$cancellation->__get('mcs_or_idx').' not stored: '.$exception->getMessage(), 'postback');
            }
        }

        if($e)
        {
            $this->event_dispatcher->dispatch($e::NAME, $e);
            $this->logger->info('Event '.$e::NAME.' dispatched.');

            $event_response = $e->getResponse();
            if($event_response)
            {
                $event->setResponse($event_response);
            }
        }
    }
}