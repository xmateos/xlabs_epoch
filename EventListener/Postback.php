<?php

namespace XLabs\EpochBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\EpochBundle\Services\Firewall;
use XLabs\EpochBundle\Services\Logger as EpochLogger;
use XLabs\EpochBundle\Request\Request as EpochRequest;
use XLabs\EpochBundle\Services\MailNotification as EpochMailNotification;
use XLabs\EpochBundle\Event\OnFlexpostPostback;
use XLabs\EpochBundle\Event\OnMemberplusPostback;
use XLabs\EpochBundle\Event\OnDataplusPostback;
use XLabs\EpochBundle\Event\OnCamchargePostback;
use XLabs\EpochBundle\Event\OnReactivationPostback;
use XLabs\EpochBundle\Event\TransactionDeclined;
use Symfony\Component\HttpFoundation\Response;

class Postback
{
    private $config;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EventDispatcherInterface $event_dispatcher, Firewall $firewall, EpochLogger $logger, EpochMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onEpochPostback(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if($request->get('_route') == 'xlabs_epoch_postback')
        {
            $querystring = $request->getQueryString();
            $querystring = empty($querystring) ? http_build_query($request->request->all()) : $querystring;

            $postback_url = $this->config['url'].$this->config['postback_url'].'?'.$querystring;

            $epochRequest = new EpochRequest($querystring);

            // Enable this line to get the generic postback (for testing purposes)
            //$this->mail_notification->send($epochRequest, '');

            $response = false;
            if(!$this->firewall->epochIpAllowed())
            {
                $content = 'IP restricted';
                $this->logger->error('IP restricted: '.trim($_SERVER['REMOTE_ADDR']).PHP_EOL.$postback_url, 'firewall');
            } else {
                $data = $epochRequest->getData();

                if(isset($data['ets_pi_code']) || isset($data['mcs_or_idx']))
                {
                    // Dataplus callback (EpochTransStats and MemberCancelStats)
                    $e = new OnDataplusPostback($epochRequest);
                } else {
                    //if($epochRequest->isApproved() && !$epochRequest->isTestTransaction())
                    if($epochRequest->isApproved())
                    {
                        if(isset($data['x_epochCamcharge']))
                        {
                            // Camcharge callback
                            $e = new OnCamchargePostback($epochRequest);
                        } elseif(isset($data['x_epochMemberplus'])) {
                            // Memberplus callback
                            $e = new OnMemberplusPostback($epochRequest);
                        } elseif(isset($data['x_epochReactivation'])) {
                            // Reactivation callback
                            $e = new OnReactivationPostback($epochRequest);
                        } else {
                            // Flexpost callback
                            $e = new OnFlexpostPostback($epochRequest);
                        }
                    } else {
                        $data['msg'] = $epochRequest->getDescriptiveError();
                        $e = new TransactionDeclined($data);
                    }
                }
                $this->event_dispatcher->dispatch($e::NAME, $e);
                $response = $e->getResponse();

                $content = $response ? $response : 'OK'; // force 'OK' response if non is given
            }

            $response = new Response();
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);
            $response->setContent($content);
            $event->setResponse($response);
        }
    }
}