<?php

namespace XLabs\EpochBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use \DateTime;

class EpochRoutingLoader extends Loader
{
    private $config;
    private $loaded = false;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "xlabs_epoch_routing" loader twice');
        }

        $routes = new RouteCollection();
        $aRoutes = array();

        // Route for postbacks
        $path = $this->config['postback_url'];
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Epoch:postback',
        );
        /*$requirements = array(
            'parameter' => '\d+',
        );*/
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_epoch_postback';
        $aRoutes[$routeName] = $route;

        // Route for epoch return urls
        $path = $this->config['return_url'];
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Epoch:response',
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_epoch_response';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats with no date set
        $path = $this->config['stats_url'];
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:index'
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats by date
        $path = $this->config['stats_url'].'/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:indexByDate',
            /*'initDate' => '%first_date_of_month%',
            'endDate' => '%last_date_of_month%',*/
            'initDate' => date('Y-m-01'),
            'endDate' => date('Y-m-t')
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_by_date';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (joins)
        $path = $this->config['stats_url'].'/joins/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:joins',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_joins';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (channel joins)
        $path = $this->config['stats_url'].'/joins/channel/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:channelJoins',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_channel_joins';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (rebills)
        $path = $this->config['stats_url'].'/rebills/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:rebills',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_rebills';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (channel rebills)
        $path = $this->config['stats_url'].'/rebills/channel/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:channelRebills',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_channel_rebills';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (purchases)
        $path = $this->config['stats_url'].'/purchases/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:purchases',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_purchases';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (refunds)
        $path = $this->config['stats_url'].'/refunds/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:refunds',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_refunds';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (channel refunds)
        $path = $this->config['stats_url'].'/refunds/channel/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:channelRefunds',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_channel_refunds';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (chargebacks)
        $path = $this->config['stats_url'].'/chargebacks/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:chargebacks',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_chargebacks';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (channel chargebacks)
        $path = $this->config['stats_url'].'/chargebacks/channel/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:channelChargebacks',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_channel_chargebacks';
        $aRoutes[$routeName] = $route;

        // Route for epoch stats (cancels)
        $path = $this->config['stats_url'].'/cancels/{initDate}/{endDate}';
        $defaults = array(
            '_controller' => 'XLabsEpochBundle:Stats:cancels',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_epoch_stats_cancels';
        $aRoutes[$routeName] = $route;

        foreach($aRoutes as $routeName => $route)
        {
            $routes->add($routeName, $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'xlabs_epoch_routing' === $type;
    }
}