<?php

namespace XLabs\EpochBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UpdateEpochIpsCommand extends Command
{
    /*
     * When the Spyder API has returned an error, but the media is in proper user folder, use this command to send it to spyder again
     */
    protected function configure()
    {
        $this
            ->setName('xlabs:epoch:update_ips')
            ->setDescription('Pulls epoch IPs to allow transactions from them.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();

        $url = 'https://epoch.com/ip_list.php';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT , 443);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);

        $response = curl_exec($ch);
        if(curl_error($ch))
        {
            mail('xavi.mateos@manicamedia.com', 'Epoch IPs updater ('.date('d-m-Y H:i').')', 'Couldnt pull the Epoch IPs');
            return curl_error($ch);
        }
        curl_close($ch);

        $ips = explode('|', trim($response));
        array_walk($ips, function (&$value, $key) {
            if(substr($value, -1) == '.')
            {
                $value = $value.'0/24';
            }
        });

        // Create TXT file
        $web_folder = $container->get('kernel')->getRootDir().'/../web/';
        $ips_file = $web_folder.'epoch_allowed_ips.txt';
        $file = fopen($ips_file, 'w');
        fwrite($file, implode(', ', $ips));
        fclose($file);
        @chmod($ips_file, 0777);
    }
}