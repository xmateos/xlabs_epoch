<?php

namespace XLabs\EpochBundle\Event;

class OnCamchargePostback extends Postback
{
    const NAME = 'epoch_postback.OnCamchargePostback.event';
}