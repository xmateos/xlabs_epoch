<?php

namespace XLabs\EpochBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Postback extends Event
{
    const NAME = 'xlabs_epoch_postback.event';

    protected $params;
    protected $response;

    public function __construct($params)
    {
        $this->params = $params;
        $this->response = false;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($name)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : false;
    }

    public function getQuerystring()
    {
        return http_build_query($this->getParams());
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }
}