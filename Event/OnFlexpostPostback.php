<?php

namespace XLabs\EpochBundle\Event;

class OnFlexpostPostback extends Postback
{
    const NAME = 'epoch_postback.OnFlexpostPostback.event';
}