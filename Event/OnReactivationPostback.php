<?php

namespace XLabs\EpochBundle\Event;

class OnReactivationPostback extends Postback
{
    const NAME = 'epoch_postback.OnReactivationPostback.event';
}