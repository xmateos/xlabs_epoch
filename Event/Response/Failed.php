<?php

namespace XLabs\EpochBundle\Event\Response;

use XLabs\EpochBundle\Event\Postback;

class Failed extends Postback
{
    const NAME = 'epoch.response_failed.event';
}