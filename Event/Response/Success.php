<?php

namespace XLabs\EpochBundle\Event\Response;

use XLabs\EpochBundle\Event\Postback;

class Success extends Postback
{
    const NAME = 'epoch.response_success.event';
}