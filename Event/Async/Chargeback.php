<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Chargeback extends Postback
{
    const NAME = 'epoch.async.chargeback.event';
}