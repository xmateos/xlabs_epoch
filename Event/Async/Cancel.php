<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Cancel extends Postback
{
    const NAME = 'epoch.async.cancel.event';
}