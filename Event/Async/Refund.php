<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Refund extends Postback
{
    const NAME = 'epoch.async.refund.event';
}