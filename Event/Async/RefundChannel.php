<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class RefundChannel extends Postback
{
    const NAME = 'epoch.async.refund_channel.event';
}