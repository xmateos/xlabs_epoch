<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Purchase extends Postback
{
    const NAME = 'epoch.async.purchase.event';
}