<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Rebill extends Postback
{
    const NAME = 'epoch.async.rebill.event';
}