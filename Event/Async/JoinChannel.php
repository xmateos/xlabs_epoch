<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class JoinChannel extends Postback
{
    const NAME = 'epoch.async.join_channel.event';
}