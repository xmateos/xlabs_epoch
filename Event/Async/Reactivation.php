<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Reactivation extends Postback
{
    const NAME = 'epoch.async.reactivation.event';
}