<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class ChargebackChannel extends Postback
{
    const NAME = 'epoch.async.chargeback_channel.event';
}