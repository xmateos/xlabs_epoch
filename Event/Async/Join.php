<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class Join extends Postback
{
    const NAME = 'epoch.async.join.event';
}