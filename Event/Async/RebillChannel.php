<?php

namespace XLabs\EpochBundle\Event\Async;
use XLabs\EpochBundle\Event\Postback;

class RebillChannel extends Postback
{
    const NAME = 'epoch.async.rebill_channel.event';
}