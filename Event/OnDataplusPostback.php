<?php

namespace XLabs\EpochBundle\Event;

class OnDataplusPostback extends Postback
{
    const NAME = 'epoch_postback.OnDataplusPostback.event';
}