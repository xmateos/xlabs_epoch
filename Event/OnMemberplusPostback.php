<?php

namespace XLabs\EpochBundle\Event;

class OnMemberplusPostback extends Postback
{
    const NAME = 'epoch_postback.OnMemberplusPostback.event';
}