<?php

namespace XLabs\EpochBundle\Event\Sync;
use XLabs\EpochBundle\Event\Postback;

class Purchase extends Postback
{
    const NAME = 'epoch.sync.purchase.event';
}