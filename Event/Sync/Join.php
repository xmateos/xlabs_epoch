<?php

namespace XLabs\EpochBundle\Event\Sync;
use XLabs\EpochBundle\Event\Postback;

class Join extends Postback
{
    const NAME = 'epoch.sync.join.event';
}