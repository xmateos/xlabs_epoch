<?php

namespace XLabs\EpochBundle\Event\Sync;
use XLabs\EpochBundle\Event\Postback;

class JoinChannel extends Postback
{
    const NAME = 'epoch.sync.join_channel.event';
}