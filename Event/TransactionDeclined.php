<?php

namespace XLabs\EpochBundle\Event;

class TransactionDeclined extends Postback
{
    const NAME = 'epoch.error.event';
}